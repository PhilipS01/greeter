import datetime
import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from decouple import config

# If modifying these scopes, delete the file googlecal_token.json.
SCOPES = ["https://www.googleapis.com/auth/calendar"]
cal_id = config("CAL_ID")


def addVeranstaltung(veranstaltung):
    """Fuege dem Google-Kalender eine Veranstaltung hinzu.

    Args:
        veranstaltung: Ein JSON-Objekt, welches "name", "ort", "lehrende", sowie "zeit" mit "start" und "ende" enthaelt.
    """
    creds = None
    # The file googlecal_token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists("googlecal_token.json"):
        creds = Credentials.from_authorized_user_file("googlecal_token.json", SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                "googlecal_credentials.json", SCOPES
            )
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open("googlecal_token.json", "w") as token:
            token.write(creds.to_json())

    try:
        service = build("calendar", "v3", credentials=creds)

        # calendar_list = service.calendarList().get(calendarId=cal_id).execute()

        event = {
            "summary": veranstaltung["name"],
            "location": veranstaltung["ort"],
            "description": veranstaltung["lehrende"],
            "start": {
                # format: "2023-09-27T09:00:00"
                "dateTime": veranstaltung["zeit"]["start"],
                "timeZone": "GMT+01:00",
            },
            "end": {
                "dateTime": veranstaltung["zeit"]["ende"],
                "timeZone": "GMT+01:00",
            },
        }

        event = service.events().insert(calendarId=cal_id, body=event).execute()
        print("Event wurde dem Kalendar hinzugefuegt.")

    except HttpError as error:
        print("Ein Problem ist aufgetreten: %s" % error)
