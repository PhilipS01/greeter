from flask import Flask

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'jht832utj[2u0utujXZ\QWRjajioujhJJJijdajjaf(£t)]'
    
    from .views import views

    app.register_blueprint(views, url_prefix='/')

    return app