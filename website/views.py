from flask import Blueprint, render_template, jsonify
from wordofday import getWordofDay
from weather import getWeather
from notion import getToDos
from random import randint
from dateutil.parser import parse
from tucan_scraper import getStundenplan

views = Blueprint("views", __name__)


@views.route("/")
def home():
    weather = getWeather()
    return render_template(
        "home.html",
        word=getWordofDay(),
        weather=weather,
        todos=getToDos(),
        bg_image=getBgImage(
            weather_description=weather[3], time=weather[0], sunrise_sunset=weather[4]
        ),
    )


@views.route("/_getHomeData", methods=["GET"])
def getHomeData():
    weather = getWeather()
    bg_image = getBgImage(
        weather_description=weather[3], time=weather[0], sunrise_sunset=weather[4]
    )

    return jsonify(
        time=weather[0],
        date=weather[1],
        temp=weather[2],
        temp_desc=weather[3],
        bg_image=bg_image,
    )


@views.route("/_getWOTD", methods=["GET"])
def getWOTD():
    return jsonify(word=getWordofDay())


@views.route("/_getToDos", methods=["GET"])
def getToDo():
    return jsonify(todos=getToDos())


@views.route("/_getStundenplan", methods=["GET"])
def getStundenplanTable():
    stundenplan = getStundenplan()
    return jsonify([str(stundenplan[0]), str(stundenplan[1])])


def getBgImage(weather_description, time, sunrise_sunset):
    font_color = "#fff"
    # check if it's night time
    if parse(time) > parse(sunrise_sunset[1].text) or parse(time) < parse(
        sunrise_sunset[0].text
    ):
        # check if sunrise is less than 30 mins away --> sunrise
        if (parse(sunrise_sunset[0].text) - parse(time)) <= (
            parse("0:30:00") - parse("0:00:00")
        ) and (parse(sunrise_sunset[0].text) - parse(time)) > (
            parse("0:0:00") - parse("0:00:00")
        ):
            # sunrise time
            switch = {
                "Heiter": "sunrise.png",
                "Klar": "sunrise.png",
                "Wolkig": f"sunrise_clouded{randint(1,2)}.png",
                "Stark bewölkt": f"night_clouded{randint(1,2)}.png",
                "Regenschauer": f"sunrise_clouded{randint(1,2)}.png",
                "Bedeckt": f"sunrise_clouded{randint(1,2)}.png",
                "Gewitter": f"thunderstorm{randint(1,3)}.png",
                "Regen": f"sunrise_clouded{randint(1,2)}.png",
                "Schauer in der Nähe": f"sunrise_clouded{randint(1,2)}.png",
            }
        else:
            # night time
            switch = {
                "Heiter": f"night_clear{randint(1,2)}.png",
                "Klar": f"night_clear{randint(1,2)}.png",
                "Wolkig": f"night_clouded{randint(1,2)}.png",
                "Stark bewölkt": f"night_clouded{randint(1,2)}.png",
                "Bedeckt": f"night_clouded{randint(1,2)}.png",
                "Regenschauer": f"night_clouded{randint(1,2)}.png",
                "Gewitter": f"thunderstorm{randint(1,3)}.png",
                "Regen": f"sunrise_clouded{randint(1,2)}.png",
                "Schauer in der Nähe": f"sunrise_clouded{randint(1,2)}.png",
            }
            font_color = "#fff"
    else:
        # check if sunset time is less than 30 mins away --> sunset
        if (parse(sunrise_sunset[1].text) - parse(time)) <= (
            parse("0:30:00") - parse("0:00:00")
        ):
            # sunset time
            switch = {
                "Sonnig": "sunset.png",
                "Heiter": "sunset.png",
                "Wolkig": f"sunset_clouded{randint(1,2)}.png",
                "Bedeckt": "mostly_clouded.png",
                "Stark bewölkt": "mostly_clouded.png",
                "Regenschauer": "heavily_clouded.png",
                "Gewitter": f"thunderstorm{randint(1,3)}.png",
                "Regen": f"sunrise_clouded{randint(1,2)}.png",
                "Schauer in der Nähe": f"sunrise_clouded{randint(1,2)}.png",
            }
        else:
            # day time
            switch = {
                "Sonnig": "clear.png",
                "Heiter": "clear.png",
                "Wolkig": f"clouded{randint(1,2)}.png",
                "Bedeckt": "mostly_clouded.png",
                "Stark bewölkt": "mostly_clouded.png",
                "Regenschauer": "heavily_clouded.png",
                "Gewitter": f"thunderstorm{randint(1,3)}.png",
                "Regen": f"sunrise_clouded{randint(1,2)}.png",
                "Schauer in der Nähe": f"sunrise_clouded{randint(1,2)}.png",
            }

    return [switch.get(weather_description, "Error"), font_color]
