from decouple import config
from notion_client import Client
from pprint import pprint
from datetime import datetime

token = config("NOTION_TOKEN")
database_id = config("NOTION_DB_ID")


def getToDos():
    r"""Gets todo-list data from notion.

    :return: array of todo-data:
        [0] = deadline; [1] = name; [2] = priority; [3] = deadline time
    """

    todos_list = []
    notion = Client(auth=token)

    todos = (
        notion.databases.query(
            **{
                "database_id": database_id,
                "filter": {
                    "or": [
                        {"property": "Status", "select": {"equals": "To Do"}},
                        {"property": "Status", "select": {"equals": "Doing"}},
                    ]
                },
            }
        ),
    )
    result_dict = todos[0]
    list_result = result_dict["results"]

    for todo in list_result:
        mapNotionResultToToDo(todo, todos_list)

    return todos_list


def mapNotionResultToToDo(result, todos):
    properties = result["properties"]

    # status = properties['Status']["select"]["name"]
    name = properties["Name"]["title"][0]["text"]["content"]

    try:
        deadline = properties["Deadline"]["date"]["start"]
        if "T" in deadline:
            deadline_date = deadline.split("T")[0]
            deadline_time = deadline.split("T")[1]

            if "+" in deadline_time:
                deadline_time = deadline_time.split("+")[0]

            deadline_time = datetime.strptime(deadline_time, "%H:%M:%S.000").strftime(
                "%H:%M"
            )

        else:
            deadline_date = deadline
            deadline_time = ""

        deadline_date = datetime.strptime(deadline_date, "%Y-%m-%d").strftime("%d. %B")
    except:
        deadline_date = ""
        deadline_time = ""

    try:
        priority = properties["Priority"]["select"]["name"]
    except:
        priority = ""

    todos.append((deadline_date, name, priority, deadline_time))
