import requests
from bs4 import BeautifulSoup
from time import localtime, strftime
from locale import setlocale, LC_ALL

setlocale(LC_ALL, "de_DE.UTF-8")

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"
}

Darmstadt = "https://weather.com/de-DE/wetter/heute/l/49.88,8.66"


def getWeather():
    r"""Gets all weather data.

    :return: array of weather-data:
        [0] = time; [1] = date; [2] = temperature; [3] = weather description; [4] = sunrise and sunset
    """

    page = requests.get(Darmstadt, headers=headers)
    soup = BeautifulSoup(page.text, "html.parser")

    time = strftime("%H:%M", localtime())
    date = strftime("%A %d. %B", localtime())
    temperature = soup.find(
        "span", {"class": "CurrentConditions--tempValue--MHmYY"}
    ).text
    description = soup.find(
        "div", {"class": "CurrentConditions--phraseValue--mZC_p"}
    ).text

    sunrise_sunset = soup.findAll("p", {"class": "TwcSunChart--dateValue--2WK2q"})

    data = [time, date, temperature, description, sunrise_sunset]

    return data
