import requests
from bs4 import BeautifulSoup
from time import localtime, strftime, strptime
from locale import setlocale, LC_ALL
import mechanize
import http.cookiejar as cookielib
from decouple import config
import re
from tucan_cal_sync import addVeranstaltung
from os import path
import json
from hashlib import md5
from datetime import timedelta, datetime, date

setlocale(LC_ALL, "de_DE.UTF-8")
tucan_user = config("TUCAN_USER")
tucan_pass = config("TUCAN_PASS")

# Browser
br = mechanize.Browser()

# Cookie Jar
cj = cookielib.LWPCookieJar()
br.set_cookiejar(cj)

# Browser options
br.set_handle_equiv(True)
br.set_handle_gzip(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
br.addheaders = [("User-agent", "Chrome")]

TD_STYLE = (
    "padding: 15px; text-align: center; background-color: rgba(255, 255, 255, 0.15);"
)


def getPrimumTimeMorgen(cookies: dict, headers: dict, arg_n_number: str):
    """Scrape TUCaN nach der ersten Veranstaltung am jeweils nächsten Tag."""
    date_url_tomorrow = date.today() + timedelta(days=1)
    date_url_tomorrow = strftime(
        "%d.%m.%Y", strptime(str(date_url_tomorrow), "%Y-%m-%d")
    )
    date_table_tomorrow = strftime(
        "%a, %-d. %b. %Y", strptime(str(date_url_tomorrow), "%d.%m.%Y")
    )
    # date_url_tomorrow = "18.10.2023"    # for demo purposes

    response = requests.get(
        f"https://www.tucan.tu-darmstadt.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=SCHEDULERPRINT&ARGUMENTS=-N{arg_n_number},-N000268,-A{date_url_tomorrow},-A,-N1",
        cookies=cookies,
        headers=headers,
    )

    soup = BeautifulSoup(response.content.decode("utf-8"), "html.parser")

    table = soup.find("table", {"class": "tb"})
    weekdays = table.find_all("td", {"class": "tbhead"})

    for day in weekdays:
        if day.text == date_table_tomorrow:
            siblings = day.parent.find_next_siblings("tr")
            for sibling in siblings:
                if sibling.findChild("td", {"class": "tbhead"}) != None:
                    for tbshd in soup.find_all("td", {"class": "tbsubhead"}):
                        tbshd.decompose()
    try:
        time = siblings[1].find(text=re.compile(r"\d{2}:\d{2} - \d{2}:\d{2}"))
        if time != None:
            return time.replace(" ", "").split("-")[0]
        else:
            return "None"
    except:
        return "None"


def getStundenplan():
    """Scrape TUCaN nach dem Stundenplan des aktuellen Datums."""
    date_url = strftime("%d.%m.%Y", localtime())
    date_table = strftime("%a, %-d. %b. %Y", localtime())

    # date_url = "17.10.2023"     # for demo purposes
    # date_table = "Di, 17. Okt. 2023"    # for demo purposes

    br.open(
        "https://www.tucan.tu-darmstadt.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=EXTERNALPAGES&ARGUMENTS=-N000000000000001"
    )

    br.select_form(nr=0)

    br.form["usrname"] = tucan_user
    br.form["pass"] = tucan_pass

    br.submit()

    new_url = br.geturl()
    arg_n_number = re.search(r"ARGUMENTS=-N(\d+)", new_url).group(1)

    br.open(
        f"https://www.tucan.tu-darmstadt.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=SCHEDULERPRINT&ARGUMENTS=-N{arg_n_number},-N000268,-A{date_url},-A,-N1"
    )
    for c in br._ua_handlers["_cookies"].cookiejar:
        cookie_val = c.value

    ### scrape website
    cookies = {
        "cnsc": cookie_val,
    }

    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "de,en-US;q=0.7,en;q=0.3",
        # 'Accept-Encoding': 'gzip, deflate, br',
        "Referer": "https://www.tucan.tu-darmstadt.de/",
        "DNT": "1",
        "Connection": "keep-alive",
        # 'Cookie': 'cnsc=8BC5820717BD80F9DF6D41EDF156EFB6',
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-User": "?1",
    }

    response = requests.get(
        f"https://www.tucan.tu-darmstadt.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=SCHEDULERPRINT&ARGUMENTS=-N{arg_n_number},-N000268,-A{date_url},-A,-N1",
        cookies=cookies,
        headers=headers,
    )

    soup = BeautifulSoup(response.content.decode("utf-8"), "html.parser")

    try:
        ## style the table
        # remove styling from html
        for tag in soup():
            for attribute in ["style", "width", "height"]:
                del tag[attribute]

        # add own styling
        for tbshd in soup.find_all("td", {"class": "tbsubhead"}):
            tbshd["style"] = "text-align: center; padding: 12px; font-size: 15px;"

        for tbdata in soup.find_all("td", {"class": "tbdata"}):
            tbdata["style"] = TD_STYLE

        table = soup.find("table", {"class": "tb"})

        weekdays = table.find_all("td", {"class": "tbhead"})

        trs = []
        for day in weekdays:
            if day.text == date_table:
                siblings = day.parent.find_next_siblings("tr")
                for sibling in siblings:
                    if sibling.findChild("td", {"class": "tbhead"}) != None:
                        for tbshd in soup.find_all("td", {"class": "tbsubhead"}):
                            tbshd.decompose()

                        break  # found next tbhead (next day)

                    else:
                        # only list first prof
                        profs = sibling(text=re.compile("\w+\.\s.*"))
                        if profs != []:
                            first_prof = soup.new_tag("span")
                            first_prof_full = str(profs[0].split(";")[0])
                            first_prof_split = first_prof_full.split(". ")

                            first_prof.string = (
                                str(first_prof_split[0])
                                + ". "
                                + str(first_prof_split[-1])
                            )
                            profs[0].replace_with(first_prof)

                        trs.append(sibling)  # add sibling to output

        # go through each table row/event
        veranstaltung_hashes = []
        veranstaltungen = []
        ### SYNC EVENTS TO GOOGLE CAL, then return table to html
        for tr in trs[1:]:
            tbdatas = tr.find_all("td", {"class": "tbdata"})
            tbdatas[0]["style"] = TD_STYLE + " border-radius: 1vw 0 0 1vw;"
            tbdatas[-1]["style"] = TD_STYLE + " border-radius: 0 1vw 1vw 0;"

            for i, col in enumerate(tr.find_all("td", {"class": "tbdata"})):
                if i == 0:  # kennung
                    # kennung auf die letzten 2 Buchstabenkürzen
                    veranstaltungs_typen = [
                        "vl",
                        "vu",
                        "ue",
                        "gü",
                        "hü",
                        "ov",
                        "iv",
                        "se",
                        "tt",
                    ]
                    for typ in veranstaltungs_typen:
                        kennung = tr(text=re.compile("-" + typ))
                        if kennung != []:
                            kennung_short = soup.new_tag("span")
                            veranstaltung_kennung = str(kennung[0].split("-")[3])
                            kennung_short.string = veranstaltung_kennung
                            kennung[0].replace_with(kennung_short)
                elif i == 1:  # event collumn
                    veranstaltung_long = tr(text=re.compile("\(.*\)"))
                    try:
                        if veranstaltung_long != []:
                            veranstaltung_short = soup.new_tag("span")
                            veranstaltung_name = str(
                                veranstaltung_long[0].split("(")[0]
                            )
                            veranstaltung_short.string = veranstaltung_name
                            veranstaltung_long[0].replace_with(veranstaltung_short)
                        else:
                            veranstaltung_name = col.decode_contents()
                    except:
                        pass
                elif i == 2:  # profs collumn
                    try:
                        veranstaltung_lehrende = (
                            str(col.find("span")).split(">")[1].split("<")[0]
                        )
                    except (
                        IndexError
                    ):  # in case there was only one prof (meaning there was no <span> element inserted)
                        veranstaltung_lehrende = col.decode_contents()
                elif i == 3:  # time collumn
                    veranstaltung_zeit = str(col.decode_contents()).replace(" ", "")
                elif i == 4:  # location collumn
                    veranstaltung_ort = col.find("a").decode_contents()
            veranstaltung = {
                "name": veranstaltung_name + " " + kennung_short.string,
                "ort": veranstaltung_ort,
                "lehrende": veranstaltung_lehrende,
                "zeit": {
                    # format required by google-cal-api: 2023-09-27T09:00:00
                    "start": strftime("%Y-%m-%d", localtime())
                    + f"T{veranstaltung_zeit.split('-')[0]}:00",
                    "ende": strftime("%Y-%m-%d", localtime())
                    + f"T{veranstaltung_zeit.split('-')[1]}:00",
                },
            }

            ## Hash der Veranstaltung speichern
            veranstaltung_hashes.append(
                int(
                    md5(
                        json.dumps(veranstaltung, sort_keys=True).encode(
                            "utf-8", errors="strict"
                        )
                    ).hexdigest(),
                    16,
                )
            )
            veranstaltungen.append(veranstaltung)

        tablehash = getTableHash(veranstaltung_hashes)
        # check if timetable has changed at all by using the table hash and updating/returning accordingly
        if path.exists("tucan_stundenplan_hash.txt"):
            with open("tucan_stundenplan_hash.txt", "r") as f:
                file_hash = f.read()
                if int(file_hash.split()[0]) == int(tablehash):
                    # add no new cal events
                    print("Es gibt keine veränderten Veranstaltungen.")
                else:
                    # file exists but table data has updated
                    with open("tucan_stundenplan_hash.txt", "w") as f2:
                        f2.write(str(tablehash))
                        addAllVeranstaltungenToCal(veranstaltungen)
        else:
            # file doesn't exist --> create
            with open("tucan_stundenplan_hash.txt", "w") as f:
                f.write(str(tablehash))
                addAllVeranstaltungenToCal(veranstaltungen)

    finally:
        if len(trs) == 0:
            trs = "None"

        primum = getPrimumTimeMorgen(
            cookies=cookies, headers=headers, arg_n_number=arg_n_number
        )

        return [trs, primum]


def getTableHash(hashes: list) -> str:
    if len(hashes) > 1:
        for i in range(len(hashes) - 1):
            hashes[i + 1] = (
                hashes[i] ^ hashes[i + 1]
            )  # XOR to combine the current and next hash -> set the next hash to new hash
        return hashes[-1]
    else:
        return hashes[0]


def addAllVeranstaltungenToCal(veranstaltungen: list):
    for veranstaltung in veranstaltungen:
        addVeranstaltung(veranstaltung=veranstaltung)
