<h1 align="center">Greeter Infotafel</h1>

<p align="center">
<img src="https://camo.githubusercontent.com/97d4586afa582b2dcec2fa8ed7c84d02977a21c2dd1578ade6d48ed82296eb10/68747470733a2f2f6261646765732e66726170736f66742e636f6d2f6f732f76312f6f70656e2d736f757263652e7376673f763d313033"/>
<img src="https://img.shields.io/pypi/pyversions/notion_client" />
<img src="https://img.shields.io/badge/license-GNU%20GPLv3-green" />
</p>

Eine self-hosted Infotafel für Wetter, TUCaN Stundenplan, Notion Erinnerungen und mehr - Ein Quelloffenes persönliches Projekt

<img src="assets/greeter_screenshot.png" />

## Hintergrund

Aufgrund der stetig wachsenden Flut an Informationen, welche wir erfahren, soll dieses Projekt ein Maß an Ordnung und Zuverlässigkeit in den Alltag bringen. Der aktuelle Zustand dieses Projektes sieht eine maßgeschneiderte Lösung für meinen persönlichen Informationsbedarf vor.</p>

## Features

Es werden folgende Informationen auf der Tafel angezeigt:

<ol style="margin-top: 1em;">
    <li>Uhrzeit und Wetter</li>
    <li>Notion Items einer bestimmten Datenbank (z.B. aktuell für Erinnerungen)</li>
    <li>Uni-Stundenplan laut TUCaN + Google Calendar Sync</li>
    <li>"Wort des Tages" zum Aufpolieren meines Englisch-Vokabulars</li>
    <li>Optionaler Video Live-Feed zu einer Kamera</li>
</ol>

## Umsetzung

### Scraping

Ein Großteil der Informationen wird mittels BeautifulSoup gescraped. Das Wetter und Wort des Tages, werden von öffentlichen Ressourcen geschürft.<br>
Nicht ganz so einfach ist es jedoch für die Notion Erinnerungen und den Uni-Stundenplan.

### Notion API

Glücklicherweise existiert eine <a href="https://pypi.org/project/notion-client/">Python Version</a> der <a href="https://github.com/makenotion/notion-sdk-js"> JavaScript SDK</a>. Unter Angabe eines Tokens und Datenbank-ID werden die Zeilen ausgelesen. Nach ein wenig Umformatieren werden die Elemente der Datenbank einem Feld/Array angefügt.

### TUCaN Scraping

TUCaN ist eine hauseigene Webapp der <a href="https://www.tu-darmstadt.de/">TU-Darmstadt</a> für die Organisation des Studiums. Oft von Ihren Nutzern verteufelt, bietet sie jedoch eine praktikable Quelle zum Scraping des Stundenplans, wie ich finde.
Doch warum sollte man überhaupt einen Stundenplan schürfen?
Gute Frage - die Antwort: weil man's kann ; ).
Aber im Ernst, so stetig wie er auch erscheinen mag, hin und wieder gibt es doch eine kleine Anpassung, wie z.B. eine einmalige Raumänderung oder ähnliches. Zugegebenermaßen passiert das nicht oft, aber es ist dennoch beruhigend sich auf die angezeigten Informationen, zumindest bezüglich ihrer Aktualität, ganz verlassen zu können.

## Hürden

### TUCaN Session Informationen

Bei einer regulären TUCaN Sitzung im Browser bekommt man nach der Anmeldung neben normalen Cookies noch Argumente in der URL, welche es für die Navigation zu berücksichtigen gibt. Mittels Regex sind diese Argumente der Form `ARGUMENTS=-N(\d+)` abzugreifen. <a href="https://github.com/python-mechanize/mechanize">Mechanize</a> wurde zum programmatischen Browsen der Seiten und für den Login benutzt.

### Google Calendar Synchronisation

Um die geschürften Daten vom Stundenplan auch effektiv in den Studentenalltag zu integrieren, werden entsprechende Ereignisse über die <a href="https://github.com/googleapis/google-api-python-client/">Google Client API</a> erstellt. Die Autorisierung erfolgt in dieser Version noch über OAUTH2 als Desktop Typ (Web application Typ wäre hier optimaler).
Um die Aktualität der Kalendereinträge zu gewährleisten, aber auch die wiederkehrende und redundante Synchronisation von bereits bestehenden Einträgen zu vermeiden, wird der aktuelle Zustand des Stundenplans bereits beim Scraping der Daten in Form eines Hashes gespeichert. Nur wenn sich die Daten im Stundenplan ändern, werden die Ereignisse hochgeladen.

## Self-Hosting

Naheliegend habe ich für's eigene Hosting zur Nginx-WSGI-Flask Kombination gegriffen, welche elegant über das Docker Image von tiangolo (<a href="tiangolo/uwsgi-nginx-flask">tiangolo/uwsgi-nginx-flask</a>) hochgefahren wird. Persönlich stellte sich diese Umsetzung am attraktivsten heraus - so konnte einfach ein zusätzlicher Container auf der <a href="https://sensorsiot.github.io/IOTstack/">IOTStack</a>-Instanz meines RPis angefügt werden.

## Weiterführendes

Der ganze Artikel über das Projekt steht auf meiner <a href="https://philipsi.de">Portfolio Website</a> zur Verfügung. In diesem finden Sie auch Hardware Liste & Co.

Zustätzlich finden sich im Verzeichnis _espcam_ STLs und G-code für das Gehäuse einer kleinen, kabellosen Überwachungskamera (siehe _Security_-Sektion).
