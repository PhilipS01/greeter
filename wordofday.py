import requests
from bs4 import BeautifulSoup

def getWordofDay():
    page = requests.get('https://www.merriam-webster.com/word-of-the-day')
    soup = BeautifulSoup(page.text, 'html.parser')

    word = soup.find(class_='word-header-txt').text
    definition = soup.find(class_='wod-definition-container').find('p').text
    
    complete = [word, definition]
    
    return complete